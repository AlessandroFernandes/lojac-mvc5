﻿class Produto
{
    
    async Decrementar(id)
    {
        await fetch(`/produto/Decrementar/${id}`, {
            method: 'POST'
        })
            .then(result => result.json())
            .then(data => this.AtualizacaoQuantidade(data))
            .catch(erro => console.error(erro))
    }

    async AtualizacaoQuantidade(base)
    {
        let $ = document.querySelector.bind(document);
        $("#Quantidade" + base.Id).innerHTML = base.Quantidade;
    }

}

const produto = new Produto();