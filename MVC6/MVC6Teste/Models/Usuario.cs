﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC6Teste.Models
{
    public class Usuario
    {
        public string Nome { get; set; }
        private string Senha { get; set; }

        public Usuario(string nome, string senha)
        {
            this.Nome = nome;
            this.Senha = senha;
        }
    }
}