﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC6Teste.Models
{
    public class Produto
    {
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string Nome { get; set; }
        [Range(0.0,30.000)]
        public decimal Preco { get; set; }
        public CategoriaProduto Categoria { get; set; }
        public int? CategoriaId { get; set; }
        public string Descricao { get; set; }
        public int Quantidade { get; set; }
    }
}