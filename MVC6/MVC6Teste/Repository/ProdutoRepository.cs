﻿using MVC6Teste.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace MVC6Teste.Repository
{
    public class ProdutoRepository
    {
        public IList<Produto> GetListaProdutos()
        {
            List<Produto> produtos = new List<Produto>();
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ESTOQUE"].ConnectionString))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT PR.IDPRODUTO AS ID, PR.NOME, PR.QUANTIDADE, PR.PRECO, PR.DESCRICAO, CA.NOME AS CATEGORIA ");
                    sb.Append("FROM PRODUTO AS PR ");
                    sb.Append("INNER JOIN CATEGORIA AS CA ");
                    sb.Append("ON PR.IDCATEGORIA = CA.IDCATEGORIA ");

                    var query = sb.ToString();

                    SqlCommand com = new SqlCommand(query, con);
                    con.Open();
                    SqlDataReader reader = com.ExecuteReader();
                    while (reader.Read())
                    {
                        Produto produto = new Produto();
                        produto.Id = (int)reader["ID"];
                        produto.Nome = reader["NOME"].ToString();
                        produto.Quantidade = (int)reader["QUANTIDADE"];
                        produto.Preco = (decimal)reader["PRECO"];
                        produto.Descricao = reader["DESCRICAO"].ToString();
                        produto.Categoria = new CategoriaProduto();
                        produto.Categoria.Nome = reader["CATEGORIA"].ToString();
                        produtos.Add(produto);
                    }
                    con.Close();
                }
                return produtos;
            }
            catch(Exception ex)
            {
               throw new Exception("Ocorreu um erro na busca de dados!" + ex);
            }
        }

        public void AdicionarProduto(Produto Produto)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO PRODUTO (NOME, PRECO, QUANTIDADE, DESCRICAO, IDCATEGORIA) ");
            sb.Append("VALUES (@ProdutoNome, @ProdutoPreco, @ProdutoQuantidade, @ProdutoDescricao, @ProdutoCategoriaId)");
            var query = sb.ToString();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ESTOQUE"].ConnectionString))
            using (SqlCommand com = new SqlCommand(query, con))
            {
                try
                {
                    com.Parameters.Add("@ProdutoNome", SqlDbType.VarChar, 50).Value = Produto.Nome;
                    com.Parameters.AddWithValue("@ProdutoPreco", Produto.Preco);
                    com.Parameters.AddWithValue("@ProdutoQuantidade", Produto.Quantidade);
                    com.Parameters.AddWithValue("@ProdutoDescricao", Produto.Descricao);
                    com.Parameters.AddWithValue("@ProdutoCategoriaId", Produto.CategoriaId);

                    con.Open();
                    com.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao gravar os dados!'", ex);
                }
            }
        }

        public Produto GetProduto(int ProdutoId)
        {
            Produto produto = new Produto();

            using(SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ESTOQUE"].ConnectionString))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT IDPRODUTO, PR.NOME, QUANTIDADE, CA.IDCATEGORIA, PRECO, DESCRICAO, CA.NOME AS NOMECATEGORIA ");
                sb.Append("FROM PRODUTO AS PR ");
                sb.Append("INNER JOIN CATEGORIA AS CA ");
                sb.Append("ON PR.IDCATEGORIA = CA.IDCATEGORIA ");
                sb.Append("WHERE IDPRODUTO =  @ProdutoId");
                var query = sb.ToString();

                using (SqlCommand com = new SqlCommand(query, con))
                {
                    
                    com.Parameters.AddWithValue("@ProdutoId", ProdutoId);
 
                    try
                    {
                        con.Open();
                        SqlDataReader reader = com.ExecuteReader();
                        while(reader.Read())
                        {
                            produto.Id = (int)reader["IDPRODUTO"];
                            produto.Nome = reader["NOME"].ToString();
                            produto.Preco = (decimal)reader["PRECO"];
                            produto.Descricao = reader["DESCRICAO"].ToString();
                            produto.Quantidade = (int)reader["QUANTIDADE"];
                            produto.CategoriaId = (int)reader["IDCATEGORIA"];
                            produto.Categoria = new CategoriaProduto();
                            produto.Categoria.Nome = reader["NOMECATEGORIA"].ToString();
                        }
                        con.Close();
                    }
                    catch(Exception ex)
                    {
                        throw new Exception("Erro ao executar a query de busca", ex);
                    }
                }
            }
            return produto;
        }

        public void Atualizar(Produto Produto)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ESTOQUE"].ConnectionString))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("UPDATE PRODUTO SET NOME = @ProdutoNome, QUANTIDADE = @ProdutoQuantidade, IDCATEGORIA = @ProdutoCategoriaId, DESCRICAO = @ProdutoDescricao ");
                    sb.Append("WHERE IDPRODUTO = @ProdutoId");
                    var query = sb.ToString();

                    using (SqlCommand com = new SqlCommand(query, con))
                    {
                        com.Parameters.AddWithValue("@ProdutoId", Produto.Id);
                        com.Parameters.AddWithValue("@ProdutoNome", Produto.Nome);
                        com.Parameters.AddWithValue("@ProdutoQuantidade", Produto.Quantidade);
                        com.Parameters.AddWithValue("@ProdutoCategoriaId", Produto.CategoriaId);
                        com.Parameters.AddWithValue("@ProdutoDescricao", Produto.Descricao);

                        con.Open();
                        com.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Erro ao atualizar", ex);
            }
        }
    }
}