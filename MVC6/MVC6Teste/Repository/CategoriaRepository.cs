﻿using MVC6Teste.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace MVC6Teste.Repository
{
    public class CategoriaRepository
    {
        public IList<CategoriaProduto> GetListaCategoria()
        {
            List<CategoriaProduto> Categorias = new List<CategoriaProduto>();
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ESTOQUE"].ConnectionString))
                {

                    SqlCommand com = new SqlCommand("SELECT * FROM CATEGORIA", con);
                    con.Open();
                    SqlDataReader reader = com.ExecuteReader();
                    while (reader.Read())
                    {
                        CategoriaProduto categoria = new CategoriaProduto();
                        categoria.Id = (int)reader["IDCATEGORIA"];
                        categoria.Nome = reader["NOME"].ToString();
                        Categorias.Add(categoria);
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocorreu um erro ao buscar categorias " + ex);
            }
            return Categorias;
        }
    }
}