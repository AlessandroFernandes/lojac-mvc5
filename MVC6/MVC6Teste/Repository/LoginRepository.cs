﻿using MVC6Teste.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace MVC6Teste.Repository
{
    public class LoginRepository
    {
        public Usuario Logar(string nome, string senha)
        {
            Usuario logado = null;

            using(SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ESTOQUE"].ConnectionString))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT * FROM USUARIOS WHERE NOME = @nome AND SENHA = @senha");
                var query = sb.ToString();

                using (SqlCommand com = new SqlCommand(query, con))
                {
                    com.Parameters.AddWithValue("@nome", nome);
                    com.Parameters.AddWithValue("@senha", senha);
                    
                    try
                    {
                        con.Open();
                        SqlDataReader reader = com.ExecuteReader();
                        while(reader.Read())
                        {
                            logado = new Usuario(reader["NOME"].ToString(), reader["SENHA"].ToString());
                        }
                    }
                    catch(Exception ex)
                    {
                        throw new Exception("Erro ao buscar query", ex);
                    }
                }
            }
            return logado;
        }
    }
}