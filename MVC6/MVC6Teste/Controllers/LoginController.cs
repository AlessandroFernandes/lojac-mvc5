﻿using MVC6Teste.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC6Teste.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {

            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Autenticar(string login, string senha)
        {
            var user = new LoginRepository().Logar(login, senha);

            if(user != null)
            {
                Session["LOGADO"] = user;
                return RedirectToAction("Index","Produto");
            }
            else
            {
                return View("Index");
            }
        }
    }
}