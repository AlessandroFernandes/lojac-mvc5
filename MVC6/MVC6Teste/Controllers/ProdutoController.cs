﻿using MVC6Teste.Filter;
using MVC6Teste.Models;
using MVC6Teste.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC6Teste.Controllers
{
    [AutenticacaoFilter]
    public class ProdutoController : Controller
    {
        // GET: Produto
        public ActionResult Index()
        {
            ViewBag.user = Session["LOGADO"];
            var produtos = new ProdutoRepository().GetListaProdutos();
            return View(produtos);
        }

        [Route("produto/adicionar")]
        public ActionResult Form()
        {
                ViewBag.Categoria = new CategoriaRepository().GetListaCategoria();
                ViewBag.Produto = new Produto();
                return View();         
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Adiciona(Produto produto)
        {
            var categoriaIdentificador = 1;

            if (produto.CategoriaId == categoriaIdentificador && produto.Preco < 20 && produto.CategoriaId != null)
            {
                ModelState.AddModelError("ErroPrecoCategoria", "Preço inferior á essa categória, Mínimo são R$ 20,00");
            }

            if (ModelState.IsValid)
            {
                var produtos = new ProdutoRepository();
                produtos.AdicionarProduto(produto);
                return RedirectToAction("Index", "Produto");
            }
            else
            {
                ViewBag.Categoria = new CategoriaRepository().GetListaCategoria();
                ViewBag.Produto = produto;
                return View("Form");
            }
        }

        [Route("produto/{ProdutoId}", Name = "Detalhes")]
        public ActionResult BuscarProduto(int ProdutoId)
        {
            var produto = new ProdutoRepository().GetProduto(ProdutoId);
            ViewBag.Produto = produto;
            return View();
        }

        [Route("produto/Decrementar/{ProdutoId}")]
        [HttpPost]
        public ActionResult Decrementar (int ProdutoId)
        {
            var produtoRepository = new ProdutoRepository();
            var produto = produtoRepository.GetProduto(ProdutoId);
            produto.Quantidade--;
            produtoRepository.Atualizar(produto);
            produto = produtoRepository.GetProduto(ProdutoId);
            return Json(produto, JsonRequestBehavior.AllowGet);
        }
    }
}